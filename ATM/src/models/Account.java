/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author Hadi
 */
public class Account {
    private String id;
    private String username;
    private char [] password;

    public Account(String id, String username, char [] password) {
        this.id = id;
        this.username = username;
        this.password = password;
    }

    public String getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public char[] getPassword() {
        return password;
    }
    
    
    
}
