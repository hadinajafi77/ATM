/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.Account;

/**
 *
 * @author Hadi
 */
public class AccountController {
    private Account account;
    private final File jsonFile;
    private final String root = System.getProperty("user.home") + "/.ATM/";
    
    private ObjectMapper mapper;
    private Map<String, Object> mappedAccounts = new HashMap<>();
    
            
    public AccountController(){
        jsonFile = new File(root + "accounts.json");
        mapper = new ObjectMapper();
        creatDirectories();
    }
    /**
     * Save the given account to the file
     * @param account 
     */
    public void saveAccount(Account account){
        this.account = account;
        writeAccounts();
    }
    
    //write the accounts to the file
    private void writeAccounts(){
        try {
            //write mapped data to the file
            readAccounts();
            mappedAccounts.put(account.getId(), account);
            mapper.writeValue(jsonFile, mappedAccounts);
        } catch (IOException ex) {
            Logger.getLogger(AccountController.class.getName()).log(Level.SEVERE, null, ex);
            System.err.println(ex.getMessage());
        }
    }
    
    public Map<String, Object> fetchAllAccounts(){
        readAccounts();
        return mappedAccounts;
    }
    
    /**
     * Read mapped data from the file
     */
    private void readAccounts(){
        try {
            mappedAccounts = mapper.readValue(jsonFile, new TypeReference<Map<String, Object>>() {});
        } catch (IOException ex) {
            Logger.getLogger(AccountController.class.getName()).log(Level.SEVERE, null, ex);
            System.err.println(ex.getMessage());
        }
    }
    
    private void creatDirectories() {
        File dirRoot = new File(root);
        if (!dirRoot.exists()) {
            dirRoot.mkdirs();
        }
        if (!jsonFile.exists()) {
            try {
                jsonFile.createNewFile();
            } catch (IOException ex) {
                System.err.println("Can't write to the disk\n" + ex.getMessage());
            }
        }
    }
}
